///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   03_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

#define MAX_CATS 20

using namespace std;

// Local Test Functions
void Cat::printCatName( Cat* atCat ) {
   cout << atCat->name;
}

void CatEmpire::isEmpty( CatEmpire newEmpire ) {
if( newEmpire.empty() ) 
   cout << "Empire is empty!" << endl;
else 
   cout << "Empire is NOT empty!" << endl;
}

int main() {
   
   cout << "*******Start of Unit Tests For Cat Objects!******* " << endl;
   Cat::initNames(); //Initialize list of cat names
   Cat* newCat[MAX_CATS];

   // Make 5 Cats
   for( int i = 0 ; i<20 ; i++ ) {
      newCat[i] = Cat::makeCat(); //Make a Cat and assign it a name
      Cat::printCatName(newCat[i]);
      cout << " created!" << endl;
   }

   cout << "*******Start of Unit Tests For Cat Empire BST!******* " << endl;
   
   //Empty()
   CatEmpire newEmpire;
   newEmpire.isEmpty(newEmpire);

   //addCat()
   for ( int j = 0 ; j<20 ; j++ ) {
   newEmpire.addCat( newCat[j] );
   Cat::printCatName( newCat[j] );
   cout << " added to empire!" << endl; 
   }
   newEmpire.isEmpty(newEmpire);

   //CatFamilyTree()
   newEmpire.catFamilyTree();
   
   //Alphabetized CatList()
   newEmpire.catList();
  
   //Pedigree using CatBegat()
   newEmpire.catBegat();

   return 0;
}


