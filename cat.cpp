///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   03_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}

void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
   names.erase( names.begin() + nameIndex );

	// Call setName() and return the named cat
   return new Cat( name );
}

bool CatEmpire::empty() {
   if( topCat == nullptr ) 
      return 1;
   else 
      return 0;
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat) {
   assert( atCat != nullptr );
   assert( newCat != nullptr );
   
   if( atCat->name > newCat->name ) {
      if( atCat->left == nullptr )
         atCat->left = newCat;
      else 
         addCat( atCat->left, newCat );
      }
   if ( atCat->name < newCat->name ) {
      if( atCat->right == nullptr )
         atCat->right = newCat;
      else
         addCat( atCat->right, newCat );
      }
}

void CatEmpire::addCat( Cat* newCat) {
   assert( newCat != nullptr );

   newCat->left = nullptr; // Make sure the newCat has no connections
   newCat->right = nullptr;

   if( topCat == nullptr ) { // If BST is empty set root to be newCat
      topCat = newCat;
      return;
   } 
   addCat( topCat, newCat ); 
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
   if( atCat == nullptr )
      return;
   dfsInorder( atCat->left );
   cout << atCat->name << endl;
   dfsInorder( atCat->right );
}

//void CatEmpire::dfsInorderReverse( Cat*atCat, int depth ) {}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr ) 
      return;
   
   if( atCat->right == nullptr && atCat->left == nullptr ) {
      return;
   }
   else if( atCat->right != nullptr && atCat->left != nullptr ) {
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   else if( (atCat->right != nullptr) ^ (atCat->left != nullptr) ) {
      if( atCat->right != nullptr )
         cout << atCat->name << " begat " << atCat->right->name << endl;
      if( atCat->left != nullptr )
         cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   
   dfsPreorder( atCat->left );
   dfsPreorder( atCat->right );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
   depth++;
   if( atCat == nullptr )   //Empty List
      return;

   dfsInorderReverse( atCat->right, depth );  //Traverse right side
   
   //Print Tree Format
   cout << string( 6 * (depth-1), ' ' ) << atCat->name;
   if( atCat->right == nullptr && atCat->left == nullptr ) 
      cout << endl;
   else if( atCat->right != nullptr && atCat->left != nullptr )
      cout << "<" << endl;
   else if( (atCat->right != nullptr) ^ (atCat->left != nullptr) ) {
      if( atCat->right != nullptr )
         cout << "/" << endl;
      else
         cout << "\\" << endl;
   }
   dfsInorderReverse( atCat->left, depth );   //Traverse Left side
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
   }
   dfsInorderReverse( topCat, 0 );
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
   dfsInorder( topCat );
}



void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
	dfsPreorder( topCat );
}

void getEnglishSuffix( int n ) {
   if( n < 0 ) 
      return;
   else {
      switch(n) {
         case 1: 
            cout << "1st Generation" << endl << "   ";
            break;
         case 2: 
            cout << "2nd Generation" << endl << "   ";
            break;
         case 3: 
            cout << "3rd Generation" << endl << "   ";
            break;
         default: 
            cout << n << "th Generation" << endl << "   ";
      }
   }
}

void CatEmpire::catGenerations() const {
   int level = 1;
   int i = 0;
   int queueSize = 0;
   queue<Cat*> catQueue;
   catQueue.push(topCat);
   while( !catQueue.empty() ) {
      getEnglishSuffix(level);
      i = 0;
      queueSize = static_cast<int>( catQueue.size() );
      while( i < queueSize ) {
         Cat* temp = catQueue.front();
         cout << temp->name << "  ";
         if( temp->left != nullptr )
            catQueue.push(temp->left);
         if( temp->right != nullptr ) 
            catQueue.push(temp->right);
         catQueue.pop();
         i++;
      }
      cout << endl;
      level++;
   }
}
